import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

import { Navigate } from "react-router-dom";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login() {

  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function login(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        //console.log(data.accessToken);

        if (data.accessToken !== undefined) {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Sabong Depot!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };


  return  (

    (user.id !== null) 
    ? 
    <Navigate to="/" />
  :
  <Container className="mb-5">
  <h1 className="my-5 text-center">Login</h1>
    <Row className="d-flex justify-content-center">
      <Col lg={6}>

        <>
          <Form onSubmit={(e) => login(e)}>
            <Form.Group className="mb-3" controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            {isActive ? (
              <Button variant="primary" type="submit" id="submitBtn">
                Submit
              </Button>
            ) : (
              <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            )}
          </Form>
        </>

      </Col>
    </Row>
  </Container>
 
  )
}
