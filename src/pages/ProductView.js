import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView() {
  const navigate = useNavigate();

  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);

      });
      
  }, [productId]);

  const checkout = (productId) => {

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Succesfully ordered!",
            icon: "success",
            text: `You have successfully ordered ${name}. Thank you.`
          });
          navigate("/");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <div className="d-grid gap-2">
                {
                  (user.id !== null) 
                  ? 
                  <Button
                    variant="primary"
                    size="lg"
                    onClick={() => checkout(productId)}
                  >Order </Button>
                 :
                  <Button as={Link} to="/login" variant="primary" size="lg">
                    Login to Order
                  </Button>
                
               }
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

