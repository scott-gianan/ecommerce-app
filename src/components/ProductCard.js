import { Link } from "react-router-dom";
import { Card, Button, Container, Row, Col } from "react-bootstrap";

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, stocks } = productProp;

  return (
    <Container>

    <Row className ="d-flex justify-content-center">

      <Col lg={6}>

        <Card className="p-3 my-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description: </Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price: </Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Text>Stocks: {stocks}</Card.Text>
            <Button as={Link} to={`/products/${_id}`} variant="primary">
              Details
            </Button>
          </Card.Body>
        </Card>

      </Col>

    </Row>

    </Container>

  );
}
