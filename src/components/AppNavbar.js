import { NavLink } from "react-router-dom";
import { useState, useContext } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";

import UserContext from "../UserContext";

export default function AppNavbar() {

  const { user } = useContext(UserContext);
  console.log(user)

  return (
    <Navbar bg="dark" expand="lg" sticky="top" className="animate-navbar nav-theme ">
      <Container>

        <Navbar.Brand as={NavLink} to="/" end>
          <img
            alt=""
            src="https://toppng.com/uploads/preview/13-rooster-vector-free-download-11562888015z57e8ridnd.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>

        <Navbar.Brand as={NavLink} to="/" end className="text-white">
          Sabong Depot
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" end className="text-white">
              Home
            </Nav.Link>
            {
              (user.isAdmin)
              ?
              <Nav.Link as={NavLink} to="/admin" className="text-white">
                Admin Dashboard
              </Nav.Link>
              :
              <Nav.Link as={NavLink} to="/products" end className="text-white">
                Products
              </Nav.Link>
            }

            {
              (user.id !== null)
              ?
              <Nav.Link as={NavLink} to="/logout" end className="text-white">
                Logout
              </Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/login" end className="text-white">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register" end className="text-white">
                Register
              </Nav.Link>
              </>
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
